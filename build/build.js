'use strict'
require('./check-versions')()

process.env.NODE_ENV = 'production'

const fs = require('fs')
const ora = require('ora')
const rm = require('rimraf')
const path = require('path')
const chalk = require('chalk')
const webpack = require('webpack')
const config = require('../config')
const webpackConfig = require('./webpack.prod.conf')

let data
let api_url = null
let main = 'src/main.js'
if (typeof process.argv[2] !== 'undefined' && process.argv[2].indexOf('api-url=http') !== -1) { // custom API URL
  data = fs.readFileSync(main, 'utf-8')
  const arg = process.argv[2].split('api-url=')
  if (arg.length > 1 && (arg[1].trim().indexOf('http://') === 0 || arg[1].trim().indexOf('https://') === 0)) {
    api_url = arg[1].trim()
    fs.writeFileSync(main, data.replace('https://mui.cloud/core', api_url), 'utf-8')
  }
}

const spinner = ora('building for production...')
spinner.start()

rm(path.join(config.build.assetsRoot, config.build.assetsSubDirectory), err => {
  if (err) throw err
  webpack(webpackConfig, (err, stats) => {
    spinner.stop()
    if (err) throw err
    process.stdout.write(stats.toString({
      colors: true,
      modules: false,
      children: false, // If you are using ts-loader, setting this to true will make TypeScript errors show up during build.
      chunks: false,
      chunkModules: false
    }) + '\n\n')

    if (api_url !== null) {
      data = fs.readFileSync(main, 'utf-8')
      fs.writeFileSync(main, data.replace(api_url, 'https://mui.cloud/core'), 'utf-8')
    }

    if (stats.hasErrors()) {
      console.log(chalk.red('  Build failed with errors.\n'))
      process.exit(1)
    }

    console.log(chalk.cyan('  Build complete ' + (api_url !== null ? '(for ' + api_url + ')' : '') + '.\n'))
    console.log(chalk.yellow(
      '  Tip: built files are meant to be served over an HTTP server.\n' +
      '  Opening index.html over file:// won\'t work.\n'
    ))
  })
})
