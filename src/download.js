import store from './store'
import bus from './bus'
import utils from './utils'
import sjcl from 'sjcl'
import cryptoConversions from './crypto_conversions'
import 'idb.filesystem.js'

let vue = {}
let debug = false
let smallQuota = 1024 * 1024
let largeQuota = smallQuota * 1024 * 100
// Use Web Crypto API instead SJCL if the browser is compatible
let useWebCrypto = typeof window.crypto !== 'undefined' && (typeof window.crypto.subtle !== 'undefined' || typeof window.crypto.webkitSubtle !== 'undefined')
let cryptoSubtle = useWebCrypto ? window.crypto.subtle || window.crypto.webkitSubtle : null // eslint-disable-line no-unused-vars
window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem

class Decryption {
  constructor (fileId, filename, fromFolder, id, uid = null, fek = null) {
    // uid and fek for shared files not owned by user
    this.halt = false
    this.enc = null
    this.sPrev = null
    this.currentChunk = 0
    this.encryptedWithWebCrypto = true

    this.fid = fileId
    this.filename = filename
    this.folderId = fromFolder
    this.id = id // Transfers ID
    this.uid = uid
    this.fek = fek

    if (uid !== null && fek !== null) {
      this.cek = null
      this.target = 'dl'
    } else { // Owned by user, cek is needed
      this.cek = sessionStorage.getItem('cek')
      this.target = 'files'
      if (this.cek === null) return false
    }

    if (this.filename.length === 0) {
      this.abort()
      return false
    }

    vue.$set(store.transfers.download, this.id, {
      name: this.filename,
      pct: 0,
      error: false
    })
    if (debug) console.log('Transfers ID: ' + this.id)

    // Get number of chunks
    vue.$http.post(this.target + '/nbChunks', {uid: this.uid, filename: this.filename, folder_id: this.folderId}).then((res) => {
      this.nbChunks = parseInt(res.body.data)
      if (debug) console.log('Number of chunks: ' + this.nbChunks)
      this.read()
    }, (res) => {
      this.abort()
      return false
    })
  }

  read (chunkNb = 0, pointer = 0) {
    if (this.halt) return false
    if (debug) console.log('Requesting chunk ' + chunkNb + ', pointer at position ' + pointer)

    vue.$http.post(this.target + '/chunk', {uid: this.uid, filename: this.filename, folder_id: this.folderId, pointer: pointer}).then((res) => {
      this.currentChunk = chunkNb
      if (debug) console.log('Got chunk ' + chunkNb + ' contents')
      this.updatePct(false)
      this.handleChunk(chunkNb, res.body.pointer, res.body.data.split(':'))
    }, (res) => {
      this.abort()
      return false
    })
  }

  handleChunk (chunkNb = 0, pointer = 0, chunk, secondTry = false) { // chunk is b64encoded and splitted
    let c, s, a, i

    let unpack = (ch, secondTry = false) => {
      let unpacked = []
      if (ch.length !== 4) { // Incomplete chunk
        this.abort()
        throw new sjcl.exception.corrupt('Error :: Incomplete chunk!') // eslint-disable-line new-cap
      }
      if (secondTry && this.encryptedWithWebCrypto && !useWebCrypto) {
        this.abort()
        console.error('Error :: Unable to decrypt, file was encrypted using WebCrypto API and is not supported in your browser!')
        return false
      }
      for (let i = 0; i < ch.length; i++) {
        if (!this.encryptedWithWebCrypto || !useWebCrypto) { // Use SJCL if specified or if Web Crypto is not supported
          if (debug && i === 0) console.log('unpack with SJCL')
          this.encryptedWithWebCrypto = false
          unpacked[i] = sjcl.codec.base64.toBits(ch[i]) // Convert to bitArray
        } else { // WebCrypto
          if (debug && i === 0) console.log('unpack with WebCrypto')
          switch (i) {
            case 0: // c
              unpacked[i] = cryptoConversions.b64ToArrayBuffer(ch[i], (chunkNb + 1) === this.nbChunks)
              break
            case 1: // salt
              unpacked[i] = sjcl.codec.base64.toBits(ch[i]) // used with SJCL
              break
            default:
              unpacked[i] = cryptoConversions.b64toWord(ch[i])
              break
          }
        }
      }
      return unpacked
    }

    [c, s, a, i] = unpack(chunk, secondTry)
    if (this.halt) return false

    let oldsPrev = this.sPrev
    this.sPrev = [this.encryptedWithWebCrypto, chunk[1]] // chunk[1]: same value with SJCL and WebCrypto
    if (this.enc === null || oldsPrev[1] !== chunk[1] || oldsPrev[0] !== this.encryptedWithWebCrypto) {
      // Key derivation needed
      if (debug) console.log('Key derivation process...')
      // If uid and fek are set, then use fek instead
      let key = this.uid !== null && this.fek !== null ? this.fek : sjcl.misc.pbkdf2(this.cek, s, 7000, 256)
      if (this.encryptedWithWebCrypto) { // WebCrypto
        cryptoSubtle.importKey('raw', cryptoConversions.bitArrayToArrayBuffer(key), {
          'name': 'AES-GCM', 'length': 256
        }, true, ['encrypt', 'decrypt']).then((derivedKey) => {
          this.enc = derivedKey
          this.decrypt(chunkNb, pointer, chunk, c, i, a, secondTry)
        })
      } else { // SJCL
        this.enc = new sjcl.cipher.aes(key) // eslint-disable-line new-cap
        this.decrypt(chunkNb, pointer, chunk, c, i, a, secondTry)
      }
    } else {
      this.decrypt(chunkNb, pointer, chunk, c, i, a, secondTry)
    }
  }

  decrypt (chunkNb = 0, pointer = 0, chunk, c, i, a, secondTry = false) {
    if (this.halt) return false

    let decryptOnError = () => {
      if (!secondTry) {
        this.encryptedWithWebCrypto = !this.encryptedWithWebCrypto
        this.handleChunk(chunkNb, pointer, chunk, true)
      } else { // Already tried, error
        this.abort()
        console.error('Error :: Unable to decrypt!')
        return false
      }
    }

    try {
      if (this.encryptedWithWebCrypto) {
        cryptoSubtle.decrypt({
          name: 'AES-GCM',
          iv: i,
          additionalData: a,
          tagLength: 128
        }, this.enc, c).then((dec) => {
          this.writeChunk(chunkNb, pointer, dec)
        }).catch(() => {
          decryptOnError()
        })
      } else { // SJCL
        this.writeChunk(chunkNb, pointer, sjcl.mode.gcm.decrypt(this.enc, c, i, a, 128))
      }
    } catch (e) { // Unable to decrypt, try with other lib (second try)
      decryptOnError()
    }
  }

  writeChunk (chunkNb = 0, pointer = 0, decrypted) {
    if (debug) console.log('write chunk')
    if (this.halt) return false
    if (!this.encryptedWithWebCrypto) { // SJCL
      decrypted = cryptoConversions.fromBitArrayCodec(decrypted)
    }
    decrypted = new Uint8Array(decrypted)

    // Use FileSystem API
    window.requestFileSystem(window.PERSISTENT, largeQuota, (fs) => {
      fs.root.getFile(this.filename, {create: true}, (fileEntry) => {
        fileEntry.createWriter((fileWriter) => {
          fileWriter.onwriteend = (e) => {
            // Chunk written on FileSystem
            if (debug) console.log('Chunk ' + chunkNb + ': Write completed')
            this.updatePct(true)
            if (this.currentChunk + 1 >= this.nbChunks) { // currentChunk start at chunk 0
              // All chunks are written
              this.dl(fileEntry)
            } else {
              // Write next chunk
              this.read(this.currentChunk + 1, pointer)
            }
          }
          // Write at the end of the file
          fileWriter.seek(fileWriter.length)
          fileWriter.write(new Blob([decrypted]))
        }, this.error)
      }, () => {
        // If we can't write to filesystem, request a quota
        this.requestQuota(this.read, this.currentChunk, pointer, largeQuota)
      })
    }, this.error)
  }

  dl (fileEntry) {
    // Try to download the file (move from FileSystem to user download folder)
    if (debug) console.log('Done!')

    let start = (url, revoke = false) => {
      document.querySelector('#dl_decrypted').href = url
      document.querySelector('#dl_decrypted').download = this.filename
      document.querySelector('#dl_decrypted').click()

      // Once downloaded, remove the file from FileSystem
      setTimeout(() => {
        this.rm()
        if (revoke) {
          if (debug) console.log('Removing temp url')
          window.URL.revokeObjectURL(url)
        }
      }, 2000)
    }

    if (typeof fileEntry.file === 'function') {
      fileEntry.file((file) => {
        if (window.navigator.msSaveBlob) { // Microsoft
          window.navigator.msSaveBlob(new Blob([file]), this.filename)
        } else {
          let f = new File([file], this.filename)
          if (debug) console.log('Creating temp url')
          start(window.URL.createObjectURL(f))
        }
      }, () => {
        start(fileEntry.toURL(), true)
      })
    } else {
      start(fileEntry.toURL(), true)
    }
  }

  rm () {
    window.requestFileSystem(window.PERSISTENT, smallQuota, (fs) => {
      fs.root.getFile(this.filename, {create: false}, (fileEntry) => {
        fileEntry.remove(() => {
          if (debug) console.log('File removed')
        }, this.error)
      }, () => { // If we can't delete to filesystem, request a quota
        this.requestQuota(this.rm, null, null, smallQuota)
      })
    }, this.error)
  }

  requestQuota (fc, arg, arg2, quota) {
    if (debug) console.log('Mui cannot download contents for now. Requesting quota...')
    if (navigator.webkitPersistentStorage === undefined) {
      if (debug) console.log('Your web browser is currently not supported by Mui app')
      this.abort()
      return false
    }
    quota = quota === undefined ? smallQuota : quota
    navigator.webkitPersistentStorage.requestQuota(quota, (grantedBytes) => {
      if (grantedBytes > 0) {
        if (debug) console.log('Allowed quota')
        fc.bind(this, arg, arg2)()
      } else {
        if (debug) console.log('Denied quota')
        this.abort()
      }
    }, () => {
      if (debug) console.log('Error while requesting quota')
      this.abort()
    })
  }

  updatePct (writeCompleted = false) {
    let c = writeCompleted ? this.currentChunk + 1 : this.currentChunk + 0.5
    let pct = Math.round(c / this.nbChunks * 100)
    let row = store.transfers.download[this.id]
    if (typeof row !== 'undefined') {
      row.pct = pct > 100 ? 100 : pct
      vue.$set(store.transfers.download, this.id, row)
      if (row.pct === 100) {
        setTimeout(() => {
          vue.$delete(store.transfers.download, this.id)
        }, 800)
      }
    }
  }

  error (msg = '', msg2 = '') {
    if (debug) console.log('Error', msg, msg2)
    bus.$emit('TransfersSetError', 'download', this.id, msg)
  }

  abort () {
    // Abort encryption process
    this.halt = true
    vue.$delete(store.transfers.download, this.id)
    if (debug) console.log('aborted ' + this.filename)
  }
}

class Download {
  constructor () {
    this.dec = {}
  }

  checkAPI () {
    return window.File && window.Blob && window.requestFileSystem
  }

  dlFiles (files, v) {
    vue = v
    if (!this.checkAPI()) {
      alert(vue.$t('Transfers.fileAPI'))
      return false
    }
    store.folder.transfers = true
    store.transfers.upSelected = false
    bus.$emit('SidebarOpenTransfers')

    let dl = (i) => {
      let row = document.querySelector('#f' + files[i]) // Get filename
      if (row !== null && row.getAttribute('data-title') !== null && row.getAttribute('data-title') !== '') {
        // Transfers ID is a random string
        let id = sjcl.codec.base64.fromBits(sjcl.random.randomWords(3)).replace(/\s/g, '').replace(/\n$/, '')
        this.dec[id] = new Decryption(files[i], utils.htmlspecialcharsDecode(row.getAttribute('data-title')), store.folder.folder_id, id)
      }
    }

    let i = 0
    dl(i)
    if (files.length > 1) {
      let timer = setInterval(() => {
        dl(++i)
        if (i >= files.length - 1) {
          clearInterval(timer)
        }
      }, 500)
    }
  }

  dlSharedFile (v, fileId, filename, fromFolder, uid, fek) {
    vue = v
    if (!this.checkAPI()) {
      alert(vue.$t('Transfers.fileAPI'))
      return false
    }
    // Transfers ID is a random string
    let id = sjcl.codec.base64.fromBits(sjcl.random.randomWords(3)).replace(/\s/g, '').replace(/\n$/, '')
    this.dec[id] = new Decryption(fileId, filename, fromFolder, id, uid, fek)
  }

  abort (id) {
    this.dec[id].abort()
  }
}

export default new Download()
