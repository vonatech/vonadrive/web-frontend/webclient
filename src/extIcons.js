class ExtIcons {
  constructor () {
    this.types = {
      'file-archive': ['zip', 'tar', 'gz', 'bz', 'bz2', 'xz', 'rar', 'jar', '7z', 'lzma'],
      'file-code': [
        'php', 'html', 'htm', 'php3', 'php4', 'php5', 'java', 'css', 'scss', 'xml', 'svg', 'sql', 'c', 'cpp', 'cs', 'js', 'au3', 'asm', 'h',
        'ini', 'jav', 'p', 'pl', 'rb', 'sh', 'bat', 'py'
      ],
      'image': ['jpg', 'jpeg', 'png', 'bmp', 'gif', 'tif', 'tiff', 'jls', 'jp2', 'j2k', 'jpf', 'jpx', 'jpm', 'mj2'],
      'file-alt': ['odt', 'odp', 'ods', 'odf', 'ott', 'ots', 'otp', 'odc'],
      'file-word': ['doc', 'docx', 'dot', 'docm', 'dotx'],
      'file-powerpoint': ['ppt', 'pptx', 'pot', 'pps', 'pptm', 'potx', 'ppsx'],
      'file-excel': ['xls', 'xlt', 'xlm', 'xlsx', 'xlsm', 'xltx', 'xltm'],
      'file-pdf': ['pdf'],
      'file-audio': ['mp3', 'ogg', 'flac', 'wav', 'aac', 'm4a'],
      'file-video': ['mp4', 'avi', 'wmv', 'mpeg', 'mov', 'mkv', 'mka', 'mks', 'flv']
    }
  }

  get (filename) {
    let icon = 'file'
    let pos = filename.lastIndexOf('.')
    if (pos !== -1) {
      let ext = filename.substr(pos + 1).toLowerCase()
      for (let key of Object.keys(this.types)) {
        if (this.types[key].indexOf(ext) !== -1) {
          icon = key
          break
        }
      }
    }
    return icon
  }
}

export default new ExtIcons()
