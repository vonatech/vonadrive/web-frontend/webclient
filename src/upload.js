import store from './store'
import bus from './bus'
import rm from './rm'
import sjcl from 'sjcl'
import cryptoConversions from './crypto_conversions'

let vue = {}
let chunkSize = 512 * 1024 // Size of one chunk in B, /!\ do not change it
let debug = false
let folderOpened = false

let yesReplaceAll = false
let yesCompleteAll = false
let noAll = false
// Use Web Crypto API instead SJCL if the browser is compatible
let useWebCrypto = typeof window.crypto !== 'undefined' && (typeof window.crypto.subtle !== 'undefined' || typeof window.crypto.webkitSubtle !== 'undefined')
let cryptoSubtle = useWebCrypto ? window.crypto.subtle || window.crypto.webkitSubtle : null // eslint-disable-line no-unused-vars

class Encryption {
  constructor (file, destFolder, id) {
    this.cek = sessionStorage.getItem('cek')
    if (this.cek === null) return false
    this.est = 1.33351898 // Estimation of the difference between the file and encrypted file
    this.yes = false
    this.halt = false
    this.status = null
    this.line = 0
    this.file = file
    this.dest_folder = destFolder
    this.id = id

    if (file._relativePath !== '') {
      this.createPath(file._relativePath, this.init)
    } else {
      this.init()
    }
  }

  init (destFolder = null) {
    if (destFolder !== null) this.dest_folder = destFolder
    this.salt = sjcl.random.randomWords(4) // crypto parameter: salt - 128 bits long (a word = 32 bits)
    this.w_salt = cryptoConversions.randomWordsToCrypto(this.salt) // convert to ArrayBuffer used by WebCrypto
    this.key = sjcl.misc.pbkdf2(this.cek, this.salt, 7000, 256) // key derivation

    this.dest_filename = this.file.name.replace(/<\/?[^>]+(>|$)/g, '').replace('"', '')
    this.est_size = Math.round(this.file.size * this.est) // Estimation of encrypted file size

    this.start = 0 // Start to write at chunk x
    this.chRead = 0 // Number of chunks read
    this.chWritten = 0 // Number of chunks written
    this.bWritten = 0 // Number of B written
    this.offset = 0

    vue.$set(store.transfers.upload, this.id, {
      name: this.dest_filename,
      pct: 0,
      error: false
    })
    if (debug) console.log('Transfers ID: ' + this.id)

    if (useWebCrypto) { // Web Crypto
      cryptoSubtle.importKey('raw', cryptoConversions.bitArrayToArrayBuffer(this.key), {
        'name': 'AES-GCM', 'length': 256
      }, true, ['encrypt', 'decrypt']).then((derivedKey) => {
        this.enc = derivedKey
        this.checkStatus()
      })
    } else { // SJCL
      this.enc = new sjcl.cipher.aes(this.key) // eslint-disable-line new-cap
      this.checkStatus() // Once initialized, check status before uploading
    }
  }

  createPath (relativePath) {
    // Create folders before uploading the file
    // store.uploadPathsCache is used in order to cache folder_id for a path
    if (typeof store.uploadPathsCache[this.dest_folder + ':' + relativePath] !== 'undefined') { // Path already exists
      this.init(store.uploadPathsCache[this.dest_folder + ':' + relativePath])
      return
    }

    let splittedPath = relativePath.split('/')
    relativePath = ''

    let createFolder = (folderId, secondTry = false) => { // Create path by adding folders recursively, if the folder exists, get its id
      if (!secondTry) relativePath += splittedPath[0]
      if (typeof store.uploadPathsCache[this.dest_folder + ':' + relativePath] !== 'undefined') { // Path already exists
        createFolderSuccess(store.uploadPathsCache[this.dest_folder + ':' + relativePath])
      } else {
        vue.$http.post('folders/add', {name: splittedPath[0], folder_id: folderId, existsId: true}).then((res) => {
          createFolderSuccess(res.body.data)
        }, (res) => {
          if (res.body.message === 'exists') {
            if (typeof res.body.data === 'number') { // Folder exists
              createFolderSuccess(res.body.data)
            } else if (!secondTry) { // It may being created, retry
              if (debug) console.log('Create folder: retry')
              setTimeout(() => {
                createFolder(folderId, true)
              }, 500)
            }
          }
        })
      }
    }
    let createFolderSuccess = (folderId) => {
      store.uploadPathsCache[this.dest_folder + ':' + relativePath] = folderId
      splittedPath.shift()
      if (splittedPath.length > 0) {
        relativePath += '/'
        createFolder(folderId)
      } else { // Done
        this.init(folderId)
      }
    }
    createFolder(this.dest_folder)
  }

  complete (line) {
    if (line === null) this.abort()
    let nbTransfers = Object.keys(store.transfers.upload).length
    let completeFile = true // Initial state
    let btnCallback = (e, yes) => {
      let index = vue.$refs.messageBox.getIndexFromEvent(e)
      if (index !== false) {
        this.yes = yes
        vue.$refs.messageBox.close(index)

        if (this.yes) {
          if (completeFile) {
            this.read(line)
          } else {
            rm.rmFromFilename(this.dest_filename, () => this.read())
          }
        } else {
          this.abort()
        }
      }
    }

    if (yesCompleteAll) {
      this.read(line)
    } else if (yesReplaceAll) {
      rm.rmFromFilename(this.dest_filename, () => this.read())
    } else if (!noAll) {
      let btns = [
        {
          type: 'button',
          class: 'btn',
          value: vue.$t('Transfers.yes'),
          clickEvent (e) {
            btnCallback(e, true)
          }
        }
      ]
      if (nbTransfers > 1) {
        btns.push({
          type: 'button',
          class: 'btn',
          value: vue.$t('Transfers.yesAll'),
          clickEvent (e) {
            if (completeFile) {
              yesCompleteAll = true
            } else {
              yesReplaceAll = true
            }
            bus.$emit('uploadRerun')
          }
        })
      }
      btns.push({
        type: 'button',
        class: 'btn',
        value: vue.$t('Transfers.no'),
        clickEvent (e) {
          btnCallback(e, false)
        }
      })
      if (nbTransfers > 1) {
        btns.push({
          type: 'button',
          class: 'btn',
          value: vue.$t('Transfers.noAll'),
          clickEvent (e) {
            noAll = true
            bus.$emit('uploadRerun')
          }
        })
      }
      vue.$refs.messageBox.add({
        type: 'upload',
        title: vue.$t('Transfers.replaceCompleteFile').replace('[filename]', this.dest_filename),
        toggles: [
          {
            leftTxt: vue.$t('Transfers.complete'),
            rightTxt: vue.$t('Transfers.replace'),
            clickEvent (e) {
              completeFile = !e.target.checked
            }
          }
        ],
        btns: btns
      })
    } else {
      this.abort()
    }
  }

  replace () {
    let nbTransfers = Object.keys(store.transfers.upload).length
    let btnCallback = (e, yes) => {
      let index = vue.$refs.messageBox.getIndexFromEvent(e)
      if (index !== false) {
        this.yes = yes
        vue.$refs.messageBox.close(index)

        if (this.yes) {
          rm.rmFromFilename(this.dest_filename, () => this.read())
        } else {
          this.abort()
        }
      }
    }

    if (yesReplaceAll) {
      rm.rmFromFilename(this.dest_filename, () => this.read())
    } else if (!noAll) {
      let btns = [
        {
          type: 'button',
          class: 'btn',
          value: vue.$t('Transfers.yes'),
          clickEvent (e) {
            btnCallback(e, true)
          }
        }
      ]
      if (nbTransfers > 1) { // Not the last file
        btns.push({
          type: 'button',
          class: 'btn',
          value: vue.$t('Transfers.yesAll'),
          clickEvent (e) {
            yesReplaceAll = true
            bus.$emit('uploadRerun')
          }
        })
      }
      btns.push({
        type: 'button',
        class: 'btn',
        value: vue.$t('Transfers.no'),
        clickEvent (e) {
          btnCallback(e, false)
        }
      })
      if (nbTransfers > 1) { // Not the last file
        btns.push({
          type: 'button',
          class: 'btn',
          value: vue.$t('Transfers.noAll'),
          clickEvent (e) {
            noAll = true
            bus.$emit('uploadRerun')
          }
        })
      }
      vue.$refs.messageBox.add({
        type: 'upload',
        title: vue.$t('Transfers.replaceFile').replace('[filename]', this.dest_filename),
        btns: btns
      })
    } else {
      this.abort()
    }
  }

  checkStatus () {
    let action = () => {
      switch (this.status) {
        case 0: // Not exists
          this.read()
          break
        case 1: // Not completed
          this.complete(this.line)
          break
        case 2: // Completed
          this.replace()
          break
        default:
          this.error()
      }
    }

    if (this.status === null) {
      vue.$http.post('files/status', {filename: this.dest_filename, folder_id: this.dest_folder, filesize: this.file.size}).then((res) => {
        if (typeof res.body.data.status !== 'undefined') {
          this.status = res.body.data.status
          if (this.status === 1) this.line = res.body.data.line
          action()
        } else {
          this.error()
        }
      }, (res) => {
        this.halt = true
        if (res.body.message === 'quota') {
          this.error('Transfers.quotaExceeded')
        } else {
          this.error()
        }
      })
    } else {
      action()
    }
  }

  read (start = 0) {
    this.start = start
    this.status = 3 // Uploading
    if (debug) console.log('start reading')

    let readChunk = () => {
      if (this.halt) return false
      let r = new FileReader()
      let blob = this.file.slice(this.offset, chunkSize + this.offset)
      r.onload = (e) => { // Block loaded
        let chkLength = e.target.result.length || e.loaded
        if (e.target.error !== null || chkLength === undefined) { // An error occurred
          this.error(null, e.target.error)
          return false
        } else if (this.offset >= this.file.size) { // File totally read
          this.success()
          return true
        }
        // Handle current chunk and read next
        this.chRead++
        this.offset += chkLength
        this.handleChunk(this.chRead, e.target.result)
        readChunk()
      }
      r.readAsArrayBuffer(blob)
    }
    readChunk()
  }

  success () {
    if (this.halt) return false
    // Waiting end of the uploading process
    let timer = setInterval(() => {
      if (debug) console.log('Waiting...')
      if (this.halt) {
        clearInterval(timer)
        return false
      }
      if (this.chWritten >= this.chRead) { // Done, write "EOF" at the end of file
        clearInterval(timer)
        vue.$http.post('files/write', {filename: this.dest_filename, folder_id: this.dest_folder, data: 'EOF'}).then((res) => {
          vue.$delete(store.transfers.upload, this.id)
          if (!folderOpened && Object.keys(store.transfers.upload).length === 0) {
            folderOpened = true
            bus.$emit('FolderOpen')
          }
        }, (res) => {
          this.error()
        })
      }
    }, 1000)
  }

  handleChunk (chunkNb, chunkContent) {
    if (this.halt) return false
    if (this.start < chunkNb) { // Encrypt it
      chunkContent = new Uint8Array(chunkContent)
      chunkContent = useWebCrypto ? chunkContent.buffer : cryptoConversions.toBitArrayCodec(chunkContent)
      if (debug) console.log('Starting encryption of part ' + chunkNb)

      let pack = (...args) => {
        let t = []
        if (debug) console.log('pack with ' + (useWebCrypto ? 'WebCrypto' : 'SJCL'))
        for (let i = 0; i < args.length; i++) {
          t.push(useWebCrypto ? (i === 0 ? cryptoConversions.arrayBufferToB64(args[i]) : cryptoConversions.wordToB64(args[i])) : sjcl.codec.base64.fromBits(args[i]))
        }
        return t.join(':')
      }

      // crypto parameter
      let initVector = sjcl.random.randomWords(4)
      let aDATA = sjcl.random.randomWords(4)

      // chunk encryption
      if (useWebCrypto) { // WebCrypto
        initVector = cryptoConversions.randomWordsToCrypto(initVector)
        aDATA = cryptoConversions.randomWordsToCrypto(aDATA)
        cryptoSubtle.encrypt({
          name: 'AES-GCM',
          iv: initVector,
          additionalData: aDATA,
          tagLength: 128
        }, this.enc, chunkContent).then((encrypted) => {
          this.writeChunk(
            chunkNb,
            pack(encrypted, this.w_salt, aDATA, initVector)
          )
        })
      } else { // SJCL
        this.writeChunk(
          chunkNb,
          pack(
            sjcl.mode.gcm.encrypt(this.enc, chunkContent, initVector, aDATA, 128),
            this.salt,
            aDATA,
            initVector
          )
        )
      }
    } else {
      this.chWritten++
      this.bWritten += Math.round(chunkSize * this.est)
      this.updatePct()
      if (debug) console.log('Did not write part ' + chunkNb)
    }
  }

  writeChunk (chunkNb, chunkContent) {
    if (this.halt) return false
    let timer = setInterval(() => { // Avoiding chunk sent before previous chunk if encryption is faster
      if (this.halt) {
        clearInterval(timer)
        return false
      }
      if (chunkNb === this.chWritten + 1) {
        clearInterval(timer)
        vue.$http.post('files/write', {filename: this.dest_filename, folder_id: this.dest_folder, data: chunkContent}).then((res) => {
          this.chWritten++
          this.bWritten += chunkContent.length
          this.updatePct()
        }, (res) => { // Quota exceeded or unable to write
          this.chWritten++
          this.abort()
          return false
        })
      }
    }, 250)
  }

  updatePct () {
    let pct = Math.round(this.bWritten / this.est_size * 100)
    let row = store.transfers.upload[this.id]
    if (typeof row !== 'undefined') {
      row.pct = pct > 100 ? 100 : pct
      vue.$set(store.transfers.upload, this.id, row)
      if (row.pct === 100) {
        setTimeout(() => {
          vue.$delete(store.transfers.upload, this.id)
        }, 800)
      }
    }
  }

  error (msg = '', msg2 = '') {
    if (debug) console.log('Error', msg, msg2)
    // If a message is defined, show error on MessageBox
    if (msg !== null && msg !== '') {
      if (!vue.$refs.messageBox.hasType('transfers-' + msg)) {
        vue.$refs.messageBox.add({
          type: 'transfers-' + msg,
          title: vue.$t('Error.default'),
          txt: vue.$t(msg),
          btns: [{
            type: 'button',
            class: 'btn',
            value: 'OK',
            clickEvent: (e) => {
              let index = vue.$refs.messageBox.getIndexFromEvent(e)
              if (index !== false) vue.$refs.messageBox.close(index)
            }
          }]
        })
      }
      this.abort()
    } else {
      bus.$emit('TransfersSetError', 'upload', this.id, msg)
    }
  }

  abort () {
    // Abort encryption process
    this.halt = true
    vue.$delete(store.transfers.upload, this.id)
    if (debug) console.log('aborted ' + this.file.name)
    bus.$emit('FolderOpen')
  }
}

class Upload {
  constructor () {
    this.enc = {}
    this.files = []
    this.transfersOpened = false

    bus.$on('uploadRerun', () => {
      vue.$refs.messageBox.closeType('upload')
      let transfers = Object.keys(store.transfers.upload)
      for (let h of transfers) {
        if (typeof this.enc[h] !== 'undefined' && this.enc[h].status !== null && this.enc[h].status !== 3) {
          // Rerun files that are not currently uploading
          this.enc[h].checkStatus()
        }
      }
    })
  }

  checkAPI () {
    return window.File && window.FileReader && window.FileList && window.Blob
  }

  upFiles (data, v, items) { // 'items' is optionnal, used for webkit folder detection
    vue = v
    folderOpened = false
    if (!this.checkAPI()) {
      alert(vue.$t('Transfers.fileAPI'))
      return false
    }

    yesReplaceAll = false
    yesCompleteAll = false
    noAll = false

    this.files = []
    this.transfersOpened = false
    let readFolder = (entry) => { // Read a folder (from drag&drop feature) and add files inside with relative path
      if (entry && entry.isDirectory) {
        entry.createReader().readEntries((entries) => {
          entries.forEach((entry) => {
            if (entry.isDirectory) {
              readFolder(entry)
            } else if (entry.isFile) {
              entry.file((file) => { // FileEntry to File object
                file._relativePath = entry.fullPath.substr(0, 1) === '/' ? entry.fullPath.substr(1) : entry.fullPath // set path, webkitRelativePath is read-only
                file._relativePath = file._relativePath.substr(0, file._relativePath.lastIndexOf('/')) // remove file name
                this.startEncryption(file)
              })
            }
          })
        })
      }
    }

    for (let i = 0; i < data.length; i++) {
      // Folder detection
      if (typeof data[i].type !== 'undefined' && data[i].type !== null && data[i].type.length > 0) {
        this.startEncryption(data[i])
      } else if (items !== undefined && items !== null && typeof items[i].webkitGetAsEntry === 'function') { // Chrome, Edge, Firefox 50+
        readFolder(items[i].webkitGetAsEntry())
      } else { // Try with FileReader but does not always detect a folder
        try {
          new FileReader().readAsBinaryString(data[i].slice(0, 5))
        } catch (e) { // could not access contents, is a directory, skip
          continue
        }
        this.startEncryption(data[i])
      }
    }
  }

  startEncryption (file) {
    if (typeof file._relativePath === 'undefined') { // set relative path
      file._relativePath = ''
      if (typeof file.webkitRelativePath !== 'undefined' && file.webkitRelativePath !== '') {
        file._relativePath = file.webkitRelativePath.substr(0, file.webkitRelativePath.lastIndexOf('/'))
      }
    }

    let findDuplicate = this.files.find(f => f.name === file.name) // Duplicate detection
    if (findDuplicate !== undefined && findDuplicate._relativePath === file._relativePath) {
      return // Do not upload duplicate files (same name + same relative path)
    }

    if (!this.transfersOpened) {
      store.folder.transfers = true
      store.transfers.upSelected = true
      bus.$emit('SidebarOpenTransfers')
      this.transfersOpened = true
    }

    // Transfers ID is a random string
    let id = sjcl.codec.base64.fromBits(sjcl.random.randomWords(3)).replace(/\s/g, '').replace(/\n$/, '')
    this.files.push(file)
    this.enc[id] = new Encryption(file, store.folder.folder_id, id)
  }

  abort (id) {
    this.enc[id].abort()
  }
}

export default new Upload()
